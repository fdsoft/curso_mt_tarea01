package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Información general del Producto")
@Entity
@Table(name = "Producto")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ApiModelProperty(notes = "Código de Barras o Código Genérico para uso con lector; debe tener minimo 5 caracteres")
	@Size(min=5, message="Codigo del Producto debe tener un minimo de 5 caracteres")
	@Column(name = "code", nullable = false, length = 20)
	private String code;
	
	@ApiModelProperty(notes = "Nombres debe tener minimo 3 caracteres")
	@Size(min=3, message="Nombres debe tener un minimo de 3 caracteres")
	@Column(name = "name", nullable = false, length = 50)
	private String name;
	
	@ApiModelProperty(notes = "Marca del Producto si esta disponible.")
	//@Size(min=3, message="Apellidos debe tener un minimo de 3 caracteres")
	@Column(name = "trademark", nullable = false, length = 30)
	private String trademark;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTrademark() {
		return trademark;
	}

	public void setTrademark(String trademark) {
		this.trademark = trademark;
	}

}
