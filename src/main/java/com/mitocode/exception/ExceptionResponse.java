package com.mitocode.exception;

import java.util.Date;

public class ExceptionResponse {

	private Date timestamp;
	private String message;
	private String details;

	public ExceptionResponse(Date timestamp, String msg, String det) {	
		this.timestamp = timestamp;
		this.message = msg;
		this.details = det;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getMensaje() {
		return message;
	}

	public void setMensaje(String msg) {
		this.message = msg;
	}

	public String getDetalles() {
		return details;
	}

	public void setDetalles(String det) {
		this.details = det;
	}

}
