package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IPersonaDAO;
import com.mitocode.model.Persona;
import com.mitocode.service.IPersonaService;

@Service
public class PersonaServiceImpl implements IPersonaService {

	@Autowired
	private IPersonaDAO dao;
	
	
	@Override
	public Persona registrar(Persona pers) {
		// TODO Auto-generated method stub
		return dao.save(pers);
	}

	@Override
	public Persona modificar(Persona pers) {
		// TODO Auto-generated method stub
		return dao.save(pers);
	}

	@Override
	public void eliminar(int id) {
		
		dao.delete(id);
		// TODO Auto-generated method stub
		
	}

	@Override
	public Persona listarId(int id) {
		// TODO Auto-generated method stub
		return dao.findOne(id);
	}

	@Override
	public List<Persona> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
